#2.2
#Implement an algorithm to find the kth to last element of a singly linked list.

from linkedList import *

def returnKthtoLast(linkedList, k):
    vals = linkedList.head
    length = 0
    while vals != None:
        vals = vals.next
        length += 1
    vals = linkedList.head
    for i in range(length-k-1):
        vals = vals.next
    return(vals.data)

#list1 = singleLinkedList()
#list1.head = node(1)
#e2 = node(2)
#e3 = node(3)
#e4 = node(4)
#e5 = node(5)
#e6 = node(6)
#e7 = node(7)

#list1.head.next = e2
#e2.next = e3
#e3.next = e4
#e4.next = e5
#e5.next = e6
#e6.next = e7

#list1.traverse()
#print(returnKthtoLast(list1, 5))
