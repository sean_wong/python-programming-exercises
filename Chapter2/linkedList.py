class node():
    def __init__(self, data=None):
        self.data = data
        self.next = None

class singleLinkedList():
    def __init__(self):
        self.head = None

    def traverse(self):
        printVal = self.head
        while printVal != None:
            print(printVal.data)
            printVal = printVal.next
