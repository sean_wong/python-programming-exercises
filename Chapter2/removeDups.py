#2.1
#Write code to remove duplicates from an unsorted linked list.
#How would you solve this problem if a temporary buffer is not allowed?

from linkedList import *

def removeDups(linkedList):
    current = linkedList.head
    while current.next != None:
        runner = current
        while runner.next != None:
            previous = runner
            runner = runner.next
            if current.data == runner.data:
                previous.next = runner.next
        current = current.next

#list1 = singleLinkedList()
#list1.head = node(1)
#e2 = node(2)
#e3 = node(3)
#e4 = node(4)
#e5 = node(1)
#e6 = node(6)
#e7 = node(2)

#list1.head.next = e2
#e2.next = e3
#e3.next = e4
#e4.next = e5
#e5.next = e6
#e6.next = e7

#list1.traverse()
#removeDups(list1)
#list1.traverse()
