#2.3
#Implement an algorithm to delete a node in the moddle (i.e, any node but the first and last node, not necessarily the exact middle) of a singly linked list, given only access to that node.

#Input: the node c from the linked list a -> b -> c -> d -> e -> F
#Result: nothing is returned, but the new linked list looks like a -> b -> d -> e -> f

from linkedList import *

def deleteMiddleNode(linkedList, node):
    current = linkedList.head
    while current.next != None:
        prevVal = current
        current = current.next
        if current.data == node.data:
            prevVal.next = current.next

#list1 = singleLinkedList()
#list1.head = node(1)
#e2 = node(2)
#e3 = node(3)
#e4 = node(4)
#e5 = node(5)
#e6 = node(6)
#e7 = node(7)

#list1.head.next = e2
#e2.next = e3
#e3.next = e4
#e4.next = e5
#e5.next = e6
#e6.next = e7

#list1.traverse()
#print("\n")
#deleteMiddleNode(list1, e4)
#list1.traverse()
