#Please write a program to compress and decompress the string "hello world!hello world!hello world!hello world!".

import zlib

string = b"hello world!hello world!hello world!hello world!"

compressed = zlib.compress(string)
print(compressed)

decompressed = zlib.decompress(compressed)
print(decompressed)
