# Given a list of ratings for pros and threshold. Find out which pros should be manually reviewed

def ratingThreshold(threshold, ratings):
    review = []
    for pro in range(0, len(ratings)):
        if (sum(ratings[pro]) / len(ratings[pro])) < threshold:
            review.append(pro)
    return(review)
