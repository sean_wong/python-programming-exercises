# Given a string, replace each character by the next character in the English alphabet

import string

alphabet = string.ascii_lowercase

def alphabeticShift(string, shift):
    shifted = []
    for char in string:
        for i in range(0, len(alphabet)):
            if char == alphabet[i]:
                new_char = (i + shift) % 26
                shifted.append(alphabet[new_char])
    shifted = ''.join(shifted)
    return(shifted)
