# Given two cells on the standard chess board, determine whether they have the same color or not.

def chessBoardCellColor(a, b):
    coords = {'a': 1, 'b': 2, 'c': 3, 'd': 4,
              'e': 5, 'f': 6, 'g': 7, 'h': 8}
    sumA = coords[a[0]] + int(a[1])
    sumB = coords[b[0]] + int(b[1])
    if sumA % 2 == sumB % 2:
        return(True)
    return(False)
