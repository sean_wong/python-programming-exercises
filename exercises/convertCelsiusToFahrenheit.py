# Function to convert celsius to fahrenheit

def convertCelsiusToFahrenheit(celsius):
    return((celsius * (9/5)) + 32)
