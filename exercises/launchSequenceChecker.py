# The master launch sequence consists of several independent sequences for different systems. Your goal is to verify that all the individual system sequences are in strictly increasing order. In other words, for any two elements i and j (i < j) of the master launch sequence that belong to the same system (having systemNames[i] = systemNames[j]), their values should be in strictly increasing order (i.e. stepNumbers[i] < stepNumbers[j])

def launchSequenceChecker(systemNames, stepNumbers):
    systems = {}
    for i in range(0, len(systemNames)):
        if systemNames[i] not in systems:
            systems[systemNames[i]] = []
            systems[systemNames[i]].append(stepNumbers[i])
        else:
            systems[systemNames[i]].append(stepNumbers[i])
    for i in systems:
        current = systems[i][0]
        for j in systems[i][1:]:
            if current > j:
                return(False)
    return(True)
