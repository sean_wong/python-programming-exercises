#Define a class with a generator which can iterate the numbers, which are divisible by 7, between a given range 0 and n.

def numbers(n):
    i = 0
    j = 0
    while i < n:
        j = i
        if j % 7 == 0:
            yield(j)
        i += 1

for i in numbers(1000):
    print(i)
