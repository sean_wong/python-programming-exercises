# There are some people and cats in a house. You are given the number of legs they have all together. Return array containing every possible number of people that could be in the house sorted in ascending order.

import math

def houseOfCats(legs):
    max_cats = math.floor(legs / 4)
    humans = []
    for i in range(0, max_cats + 1):
        human_legs = legs - 4 * i
        humans.append(int(human_legs / 2))
    return(humans[::-1])
