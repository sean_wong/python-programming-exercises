# Flatten a nested array

def flattenArray(array, flattened = None):
    if flattened is None:
        flattened = []
    for i in array:
        if type(i) != list:
            flattened.append(i)
        else:
            flattenArray(i, flattened)
    return(flattened)
