# Given a divisor and a bound, find the largest integer N such that:
# - N is divisible by divisor
# - N is less than or equal to bound
# - N is greater than 0

def maxMultiple(divisor, bound):
    n = 1
    while n <= bound:
        if n % divisor == 0:
            divisible = n
        n += 1
    return(divisible)
