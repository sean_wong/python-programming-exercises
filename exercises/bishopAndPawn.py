# Given position of white bishop and black pawn on chessboard, determine whether bishop can capture the pawn in one move

def bishopAndPawn(bishop, pawn):
    coords = {'a': 1, 'b': 2, 'c': 3, 'd': 4,
              'e': 5, 'f': 6, 'g': 7, 'h': 8}
    bishopCoords = [coords[bishop[0]], int(bishop[1])]
    pawnCoords = [coords[pawn[0]], int(pawn[1])]
    x_diff = bishopCoords[0] - pawnCoords[0]
    if pawnCoords[1] + abs(x_diff) == bishopCoords[1]:
        return(True)
    elif pawnCoords[1] + x_diff == bishopCoords[1]:
        return(True)
    return(False)
