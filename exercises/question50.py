#Define a class named Circle which can be constructed by a radius. The Circle class has a method which can compute the area.

import math

class circle():
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return((self.radius**2)*math.pi)

circle1 = circle(5)
print(circle1.area())
