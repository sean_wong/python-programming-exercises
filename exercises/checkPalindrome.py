# Given the string, check if it is a palindrome.

def checkPalindrome(string):
    return(list(string) == list(string)[::-1])
