# Given string s. Is it possible to change string s to string t by removing characters from string s.

def convertString(s, t):
    possibleWord = []
    letter = 0
    for i in range(0, len(s)):
        if s[i] == t[letter]:
            possibleWord.append(s[i])
            letter += 1
    if ''.join(possibleWord) == t:
        return(True)
    return(False)
