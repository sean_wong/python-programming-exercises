# Array of integers represents coordinates of obstacles in a straight line. Only allowed to make jumps of same length. Find minimal length of jump to avoid all obstacles.

def avoidObstacles(obstacles):
    jump_length = 1
    cleared = False
    while(cleared == False):
        for i in range(0, max(obstacles) + jump_length, jump_length):
            if i in obstacles:
                jump_length += 1
                break
            elif i > max(obstacles):
                cleared = True
    return(jump_length)
