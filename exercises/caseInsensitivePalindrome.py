# Given a string, check if it can become a palindrome through a case change of some letters.

def caseInsensitivePalindrome(string):
    string = string.lower()
    if list(string) == list(string)[::-1]:
        return(True)
    return(False)
