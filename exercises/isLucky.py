# Given a ticket number n, dtermine if lucky. Number is lucky if sum of first half of digits is equal to sum of the second half.

def isLucky(ticket):
    ticket = str(ticket)
    half = int(int(len(ticket)) / 2)
    firstHalf = 0
    secondHalf = 0
    for i in range(0, half):
        firstHalf += int(ticket[i])
        secondHalf += int(ticket[i + half])
    if firstHalf == secondHalf:
        return(True)
    return(False)
