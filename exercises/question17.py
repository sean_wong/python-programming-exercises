#Write a program that computes the net amount of a bank account based a transaction log from console input. The transaction log format is shown as following:
#D 100
#W 200

netAmount = 0
while(True):
    transaction = input("Enter a transaction or nothing to stop: ")
    if transaction == "":
        break
    transaction = transaction.split(" ")
    switch = transaction[0]
    amount = int(transaction[1])
    if switch == "D":
        netAmount += amount
    elif switch == "W":
        netAmount -= amount
    else:
        print("Error, please try again.")
        pass

print(netAmount)
