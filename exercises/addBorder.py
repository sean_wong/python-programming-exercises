# Given a rectangular matrix of characters, add a border of * to it.

def addBorder(matrix):
    for i in range(len(matrix)):
        matrix[i] = "*" + matrix[i]
        matrix[i] = matrix[i] + "*"
    matrix.insert(0, "*" * len(matrix[0]))
    matrix.append("*" * len(matrix[-1]))
    return(matrix)
