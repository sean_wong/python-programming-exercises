#Define a custom exception class which takes a string message as attribute.

class customError(Exception):
    """Custom error

    Attirbutes:
        message -- error brough up for a custom reason
    """

    def __init__(self, message):
        self.message = message

string1 = "custom1"

try:
    if string1 != "custom":
        raise(customError("Incorrect"))
except customError:
    print("woah")
