# A step(x) operation works like this: it changes a number x into x - s(x), where s(x) is the sum of x's digits. You like applying functions to numbers, so given the number n, you decide to build a decreasing sequence of numbers: n, step(n), step(step(n)), etc., with 0 as the last element. Building a single sequence isn't enough for you, so you replace all elements of the sequence with the sums of their digits (s(x)). Now you're curious as to which number appears in the new sequence most often. If there are several answers, return the maximal one.

from statistics import mode

def step(n, nums = None):
    if nums is None:
        nums = []
    if n <= 0:
        nums.append(0)
        return(nums)
    else:
        nums.append(n)
        sums = 0
        while n:
            sums += n % 10
            n //= 10
        return(step(nums[-1] - sums, nums))

def mostFrequentDigitSum(sequence):
    freq = []
    for i in sequence:
        sums = 0
        while i:
            sums += i % 10
            i //= 10
        freq.append(sums)
    return(mode(freq))
