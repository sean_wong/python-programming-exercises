#Define a function which can generate a dictionary where the keys are numbers between 1 and 20 (both included) and the values are square of keys. The function should just print the values only.

def squares(n):
    squares = dict()
    for i in range(1, n):
        squares[i] = i**2
    for (key, values) in squares.items():
        print(values)

squares(17)
