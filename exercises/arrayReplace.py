# Given array of integers, replace occurences of an element with a substitute

def arrayReplace(integers, element, substitute):
    for i in range(0, len(integers)):
        if integers[i] == element:
            integers[i] = substitute
    return(integers)
