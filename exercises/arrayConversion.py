# Given an array of 2k integers, perform following operations until array contains one element:
# - On the 1st, 3rd, 5th, etc. iterations (1-based) replace each pair of consecutive elements with their sum
# - On the 2nd, 4th, 6th, etc. iterations replace each pair of consecutive elements with their product

def arrayConversion(integers):
    result = integers
    i = 1
    while len(result) != 1:
        placeholder = []
        if i % 2 == 1:
            for j in range(1, len(result), 2):
                placeholder.append(result[j-1] + result[j])
        if i % 2 == 0:
            for j in range(1, len(result), 2):
                placeholder.append(result[j-1] * result[j])
        if len(result) % 2 == 1:
            placeholder.append(result[-1])
        result = placeholder
        i += 1
    return(result)
