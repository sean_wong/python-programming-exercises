# For each trainer we collect two pieces of information per task [answerTime, correctness], where correctness is 1 if the answer was correct, -1 if the answer was wrong, and 0 if no answer was given. In this case, the bot's correct answer time for a given task would be the average of the answer times from the trainers who answered correctly. Given all of the training information for a specific task, calculate the bot's answer time.

def companyBotStrategy(data):
    answerSum = 0
    answerCorrect = 0
    for trainer in data:
        if trainer[1] == 1:
            answerSum += trainer[0]
            answerCorrect += 1
    answerTime = answerSum / answerCorrect
    return(answerTime)
