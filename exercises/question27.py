#Define a function that can convert a integer into a string and print it in console.

def intToStr(num):
    return(str(num))

number = 17
print(type(number))
strNum = intToStr(number)
print(strNum)
print(type(strNum))
