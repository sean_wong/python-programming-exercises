#A robot moves in a plane starting from the original point (0,0). The robot can move toward UP, DOWN, LEFT and RIGHT with a given steps. The trace of robot movement is shown as the following:
#UP 5
#DOWN 3
#LEFT 3
#RIGHT 2
#¡­
#The numbers after the direction are steps. Please write a program to compute the distance from current position after a sequence of movement and original point. If the distance is a float, then just print the nearest integer.

import math

position = [0, 0]

while(True):
    movement = input("Enter a direction and number of steps or nothing to finish: ")
    movement = movement.split(" ")
    if movement[0] == "UP":
        position[0] += int(movement[1])
    elif movement[0] == "DOWN":
        position[0] -= int(movement[1])
    elif movement[0] == "RIGHT":
        position[1] += int(movement[1])
    elif movement[0] == "LEFT":
        position[1] -= int(movement[1])
    else:
        break

print(int(round(math.sqrt(position[0]**2 + position[1]**2))))
