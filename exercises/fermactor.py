# Fermat's factorization method
# semiprime number

import math

def fermactor(n):
    factors = []
    for i in range(0, n + 1):
        for j in range(0, i):
            if i**2 - j**2 == n:
                factors.append([i,j])
                break
    return(factors)
