#Write a program which accepts a sequence of comma separated 4 digit binary numbers as its input and then check whether they are divisible by 5 or not. The numbers that are divisible by 5 are to be printed in a comma separated sequence.

sequence = input("Enter a sequence of 4 digit binary numbers: ")
nums = sequence.split(',')
divisible = []

for num in nums:
    if int(num, 2)%5 == 0:
        divisible.append(num)

print(','.join(divisible))
