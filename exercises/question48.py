#Define a class named American which has a static method called printNationality.

class american():
    @staticmethod
    def printNationality():
        print("American")

american = american()
american.printNationality()
