# You have deposited a specific amount of dollars into your bank account. Each yoear your balance increases at the same growth rate. Find out how long it would take for your balance to pass a specific threshold assuming no additional deposits.

def depositProfit(deposit, rate, threshold):
    years = 0
    while deposit < threshold:
        deposit *= ((rate / 100) + 1)
        years += 1
    return(years)
