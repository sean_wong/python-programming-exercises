# Given a rectangular matrix and integer column, return array containing elements of the columnth column of the matrix.

def extractMatrixColumn(matrix, column):
    extracted = []
    for i in matrix:
        for j in range(0, len(i)):
            if j == column:
                extracted.append(i[j])
    return(extracted)
