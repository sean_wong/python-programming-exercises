# At some point during the walk the boy encounters a house with number 0 written on it, which surprises him so much that he stops adding numbers to his total right after seeing that house. For the given sequence of houses determine the sum that the boy will get. It is guaranteed that there will always be at least one 0 house on the path.

def houseNumbersSum(houses):
    sumHouses = 0
    houseNum = 0
    while houses[houseNum] != 0:
        sumHouses += houses[houseNum]
        houseNum += 1
    return(sumHouses)
