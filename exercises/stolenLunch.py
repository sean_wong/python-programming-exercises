# When you recently visited your little nephew, he told you a sad story: there's a bully at school who steals his lunch every day, and locks it away in his locker. He also leaves a note with a strange, coded message. Your nephew gave you one of the notes in hope that you can decipher it for him. And you did: it looks like all the digits in it are replaced with letters and vice versa. Digit 0 is replaced with 'a', 1 is replaced with 'b' and so on, with digits replaced by 'j'. The note is different every day, so you decide to write a function that will decipher it for your nephew on an ongoing basis.

def stolenLunch(ciphertext):
    message, code = ciphertext.split(": ")
    message = list(message)
    code = list(code)
    for i in range(0, len(message)):
        if message[i].isnumeric():
            message[i] = chr(97 + int(message[i]))
    for i in range(0, len(code)):
        code[i] = str(ord(code[i]) - ord('a'))
    plaintext = ''.join(message) + ": " + ''.join(code)
    return(plaintext)
