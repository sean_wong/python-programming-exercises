# Return the factorial of the provided integer

def factorializeANumber(integer):
    if integer == 1 or integer == 0:
        return(1)
    else:
        return(factorializeANumber(integer - 1) * integer)
