# Given a string, find out if its characters can be rearranged to form a palindrome

def palindromeRearranging(string):
    counts = {}
    odd = 0
    for i in string:
        if i not in counts:
            counts[i] = 1
        else:
            counts[i] += 1
    for i in counts.values():
        if i % 2 == 1:
            odd += 1
    if odd <= 1:
        return(True)
    return(False)
