# Given array of integers, remove each kth element from it.

def extractEachKth(integers, k):
    extracted = []
    for i in range(1, len(integers) + 1):
        if i % k != 0:
            extracted.append(integers[i-1])
    return(extracted)
