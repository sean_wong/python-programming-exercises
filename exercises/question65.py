#Please write a program using list comprehension to print the Fibonacci Sequence in comma separated form with a given n input by console.

number = int(input("PLease enter a number: "))

def fibonacci(term):
    if term == 0:
        return(0)
    elif term == 1:
        return(1)
    else:
        return(fibonacci(term - 1) + fibonacci(term - 2))

# each iteration is successive value in sequence
sequence = [fibonacci(i) for i in range(0, (number + 1))]
print(sequence)
