# Given array find first duplicate number for which second occurrence has minimal index

def firstDuplicate(integers):
    duplicates = {}
    for i in integers:
        if i not in duplicates:
            duplicates[i] = 1
        else:
            return(i)
    return(-1)
