# Return pair of adjacent elements with largest product in array of integers

def adjacentElementsProduct(integers):
    largest = integers[0] * integers[1]
    for i in range(3, len(integers)):
        maybe = integers[i] * integers[i - 1]
        if maybe > largest:
            largest = maybe
    return(largest)
