# Digit degree is defined as the number of times a number needs to be replaced by the sum of its digits until a one digit number is reached. Given an integer, find its digit degree

def digitDegree(integer, degree = 0):
    if len(str(integer)) == 1:
        return(degree)
    else:
        sums = 0
        while integer:
            sums += integer % 10
            integer //= 10
        return(digitDegree(sums, degree + 1))
