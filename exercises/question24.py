#Please write a program to print some Python built-in functions documents, such as abs(), int(), raw_input() and add document for your own function

print(abs.__doc__)
print(int.__doc__)
print(input.__doc__)

def one():
    '''Returns the integer, one (1).'''
    return(1)

print(one())
print(one.__doc__)
