# Given a string, output its longest prefix which contains only digits

def longestDigitsPrefix(string):
    string = list(string)
    digits = ""
    for i in string:
        if i.isnumeric():
            digits += i
        else:
            break
    return(digits)
