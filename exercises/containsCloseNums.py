# Given array of integers nums and integer k, determine whether there are two distinct indices i and j in the array where nums[i] = nums[j] and the absolute difference between i and j is less than or equal to k

def containsCloseNums(nums, k):
    for i in range(k - 1, len(nums), k):
        if nums[i] == nums[i - k]:
            return(True)
    return(False)
