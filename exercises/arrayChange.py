# Given array of integers. Each move you are allowed to increase exactly one of its elements by one. Find minimal number of moves required to obtain a strictly increasing sequence.

def arrayChange(integers):
    moves = 0
    for i in range(1, len(integers)):
        while integers[i] <= integers[i - 1]:
            integers[i] += 1
            moves += 1
    return(moves)
