# Given a four-digit passcode. If 10 or more consecutive failed passcode attempts were made, device should be locked

def incorrectPasswordAttempts(passcode, attempts):
    incorrect = 0
    for attempt in attempts:
        if attempt == passcode:
            incorrect = 0
        else:
            incorrect += 1
        if incorrect >= 10:
            return(True)
    return(False)
