# Given a positive integer num, return the sum of all odd Fibonacci numbers that are less than or equal to num.

def sumOddFibonacciNums(num):
    fibSum = 2
    prev = 1
    new = 2
    while True:
        old = new
        new += prev
        prev = old
        if new > num:
            break
        if new % 2 == 1:
            fibSum += new
    return(fibSum)
