#Write a program that accepts sequence of lines as input and prints the lines after making all characters in the sentence capitalized.

sentences = []

while(True):
    string = input("Enter a string: ")
    if len(string) > 0:
        sentences.append(string.upper())
    else:
        break

for sentence in sentences:
    print(sentence)
