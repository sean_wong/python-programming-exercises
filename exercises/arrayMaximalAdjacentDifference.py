# Given array of integers, find maximal absolute diff between any two of its adjacent elements

def arrayMaximalAdjacentDifference(integers):
    max_diff = abs(integers[0] - integers[1])
    for i in range(2, len(integers)):
        probable_max = abs(integers[i] - integers[i-1])
        if probable_max > max_diff:
            max_diff = probable_max
    return(max_diff)
