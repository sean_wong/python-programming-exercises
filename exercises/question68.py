#Please write assert statements to verify that every number in the list [2,4,6,8] is even.

given = [2,4,6,8]

for i in given:
    assert(i % 2 == 0)
