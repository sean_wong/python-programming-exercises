# Given two strings, find the number of common characters between them.
# lowercase letters

def commonCharacterCount(a, b):
    counter = 0
    listA = list(a)
    listB = list(b)
    while(True):
        if len(listA) == 0 or len(listB) == 0:
            break
        current_letter = listA[0]
        if current_letter in listB:
            listA.remove(current_letter)
            listB.remove(current_letter)
            counter += 1
        else:
            listA.remove(current_letter)
    return(counter)
