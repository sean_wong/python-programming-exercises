# Given a list of pros and their category preferences, return the list of pros for each category.

def proCategorization(pros, preferences):
    categorize = {}
    for i in range(0, len(preferences)):
        for j in range(0, len(preferences[i])):
            if preferences[i][j] not in categorize:
                categorize[preferences[i][j]] = []
            categorize[preferences[i][j]].append(pros[i])
    return(categorize)    
