# Apply box blur algorithm to image

import math

def boxBlur(image):
    blurred = image
    for y in range(1, len(image) - 1):
        for x in range(1, len(image[0]) - 1):
            pixelSum = sum([image[y - 1][x - 1],        # top left
                            image[y - 1][x],            # top middle
                            image[y - 1][x + 1],        # top right
                            image[y][x - 1],            # middle left
                            image[y][x],                # center
                            image[y][x + 1],            # middle right
                            image[y + 1][x - 1],        # bottom left
                            image[y + 1][x],            # bottom middle
                            image[y + 1][x + 1]])       # bottom right
            blurred[y][x] = math.floor(pixelSum / 9)    # average
    return(blurred)
