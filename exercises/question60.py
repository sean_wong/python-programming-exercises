#Write a program to read an ASCII string and to convert it to a unicode string encoded by utf-8.

string1 = input("Enter a string: ")

toUnicode = unicode(string1, "utf-8") #in python3, str already has unicode
print(toUnicode)
