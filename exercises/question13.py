#Write a program that accepts a sentence and calculate the number of letters and digits.

string = input("Please enter a sentence: ")

counter = {"numbers":0, "letters":0, "other":0}
for character in string:
    if character.isdigit():
        counter["numbers"] += 1
    elif character.isalpha():
        counter["letters"] += 1
    else:
        counter["other"] += 1

print("letters", counter["letters"])
print("numbers", counter["numbers"])
print("other", counter["other"])
