# Call two arms equally strong if the heaviest weights they each are able to lift are equal.
# Call two people equally strong if their strongest arms are equally strong (the strongest arm can be both the right and the left), and so are their weakest arms.
# Given your and your friend's arms' lifting capabilities find out if you two are equally strong.

def areEquallyStrong(left, right, friendLeft, friendRight):
    if left > right:
        strongest = left
        weakest = right
    else:
        strongest = right
        weakest = left

    if friendLeft > friendRight:
        friendStrongest = friendLeft
        friendWeakest = friendRight
    else:
        friendStrongest = friendRight
        friendWeakest = friendLeft
    
    if strongest == friendStrongest and weakest == friendWeakest:
            return(True)
    return(False)
