# Sum all the prime numbers up to and including the provided number

def sumAllPrimes(n):
    total = 0
    for num in range(2, n + 1):
        i = 2
        prime = 1
        while i**2 <= num:
            if num % i == 0:
                prime = 0
            i += 1
        if prime:
            total += num
    return(total)
