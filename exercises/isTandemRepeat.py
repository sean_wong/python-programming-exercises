# Determine whether given string can be obtained by one concatenation of some string to itself

def isTandemRepeat(string):
    half = int(len(string) / 2)
    if string[0:half] == string[half:]:
        return(True)
    return(False)
