#Write a program to compute the frequency of the words from the input. The output should output after sorting the key alphanumerically.

sentence = input("Please enter a sentence: ")
sentence = sentence.split(" ")
word_count = {}

for word in sentence:
    if word not in word_count.keys():
        word_count[word] = 1
    else:
        word_count[word] += 1

for word in sorted(word_count.keys()):
    print("{}: {}".format(word, word_count[word]))
