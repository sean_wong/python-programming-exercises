#Write a program that accepts a sentence and calculate the number of upper case letters and lower case letters.

string = input("Enter a sentence: ")

counter = {"upper":0, "lower":0}
for character in string:
    if character.isupper():
        counter["upper"] += 1
    elif character.islower():
        counter["lower"] += 1
    else:
        pass

print("Uppercase", counter["upper"])
print("Lowercase", counter["lower"])
