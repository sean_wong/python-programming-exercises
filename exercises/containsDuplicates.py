# Given array of integers, write a function that determines whether the array contains any duplicates

def containsDuplicates(integers):
    uniq = {}
    for i in integers:
        if i not in uniq:
            uniq[i] = 1
        else:
            uniq[i] += 1
    if sum(uniq.values()) == len(uniq):
        return(False)
    return(True)
