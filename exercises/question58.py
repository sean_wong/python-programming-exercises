#Write a program which accepts a sequence of words separated by whitespace as input to print the words composed of digits only.

string1 = input("Enter a sentence with numbers: ").split(" ")
numbers = []

for i in string1:
    try:
        int(i)
        numbers.append(i)
    except ValueError:
        pass

print(numbers)
