# For a given list of domains return list of labels

def domainType(domains):
    labels = {"org": "organization", "com": "commercial", "net": "network", "info": "information"}
    label_list = []
    for i in domains:
        label_list.append(labels[i.split('.')[-1]])
    return(label_list)
