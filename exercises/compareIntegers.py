# Compare two integers given as strings

def compareIntegers(a, b):
    if int(a) > int(b):
        return("greater")
    elif int(a) < int(b):
        return("lesser")
    else:
        return("equal")
