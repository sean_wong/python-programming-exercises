#Please write a program using generator to print the even numbers between 0 and n in comma separated form while n is input by console.

number = int(input("Please enter a number: "))

evens = []

def even(upTo):
    i = 0
    while i < (upTo + 1):
        if i % 2 == 0:
            yield(i)
        i += 1

for i in even(number):
    evens.append(i)

print(evens)
