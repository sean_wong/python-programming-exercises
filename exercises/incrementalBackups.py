# Give a list of changes made to the files in a database, where for each valid i, changes[i][0] is the timestamp of the time the change was made, and changes [i][1] is the file id. Knowing the timestamp of the last backup lastBackupTime, your task is to find the files which should be included in the next backup. Return the ids of the files that should be backed up as an array sorted in ascending order.

def incrementalBackups(lastBackupTime, changes):
    needBackup = []
    for change in changes:
        if change[0] > lastBackupTime:
            needBackup.append(change[1])
    return(sorted(list(set(needBackup))))
