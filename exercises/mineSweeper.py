# In minesweeper show number of mines surrounding

def mineSweeper(matrix):
    around = [[-1, -1], # top left
              [-1, 0],  # top center
              [-1, 1],  # top right
              [0, -1],  # middle left
              [0, 1],   # middle right
              [1, -1],  # bottom left
              [1, 0],   # bottom middle
              [1, 1]]   # bottom right
    mineField = []
    for row in range(0, len(matrix)):
        mineRow = []
        for column in range(0, len(matrix[row])):
            mineSum = 0
            for i in range(0, 8):
                try:
                    if row + around[i][0] == -1 or column + around[i][1] == -1:
                        raise IndexError
                    mineSum += matrix[row + around[i][0]][column + around[i][1]]
                except IndexError:
                    mineSum += 0
            mineRow.append(mineSum)
        mineField.append(mineRow)
    return(mineField)
