#Define a function that can accept an integer number as input and print the "It is an even number" if the number is even, otherwise print "It is an odd number".

def isOdd(number):
    if number % 2 == 0:
        print("It is an even number")
    else:
        print("It is an odd number")

isOdd(3)
isOdd(100000)
