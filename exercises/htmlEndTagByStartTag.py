# Given starting HTML tag, find appropriate end tag

def htmlEndTagByStartTag(startTag):
    elements = startTag.split(" ")
    endTag = []
    for i in elements[0]:
        if i.isalnum():
            endTag.append(i)
    return("</" + ''.join(endTag) + ">")
