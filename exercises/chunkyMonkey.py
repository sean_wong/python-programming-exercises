# Write function that splits an array into groups the length of size k and returns them as a two dimensional array

def chunkyMonkey(array, k):
    chunked = []
    prev_val = 0
    for i in range(k, len(array) + k, k):
        chunked.append(array[prev_val:i])
        prev_val = i
    return(chunked)
