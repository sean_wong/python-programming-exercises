# Let's call product(x) the product of x's digits. Given an array of integers a, calculate product(x) for each x in a, and return the number of distinct results you get.

def uniqueDigitsProducts(array):
    uniq = []
    for integer in array:
        product = 1
        while integer:
            product *= integer % 10
            integer //= 10
        uniq.append(product)
    return(len(set(uniq)))
