#Use a list comprehension to square each odd number in a list. The list is input by a sequence of comma-separated numbers.

numbers = input("Enter a list of numbers: ")

odds = [int(num)**2 for num in numbers.split(",") if int(num) % 2 != 0]

print(odds)
