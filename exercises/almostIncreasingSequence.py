# Given array of integers, determine whether it is possible to obtain a strictly increasing sequence by removing no more than one element from the array.

def almostIncreasingSequence(integers):
    counter = 0
    previous = integers[0]
    for i in range(1, len(integers)):
        if integers[i] < previous:
            counter += 1
        previous = integers[i]
        if counter > 1:
            return(False)
    return(True)
