# fare = cost per minute * ride time + cost per mile * ride distance

def fareEstimator(ride_time, ride_distance, cost_per_minute, cost_per_mile):
    fares = []
    for i in range(0, len(cost_per_minute)):
        fare = ride_time * cost_per_minute[i] + ride_distance * cost_per_mile[i]
        fares.append(fare)
    return(fares)
