# Being a new Uber user, you have $20 off your first ride. You want to make the most out of it and drive in the fanciest car you can afford, without spending any out-of-pocket money. There are 5 options, from the least to the most expensive: "UberX", "UberXL", "UberPlus", "UberBlack" and "UberSUV". You know the length l of your ride in miles and how much one mile costs for each car. Find the best car you can afford.

def fancyRide(length, fares):
    rides = {1: "UberX", 2: "UberXL", 3: "UberPlus", 4: "UberBlack", 5: "UberSUV"}
    fancy = None
    for i in range(0, len(fares)):
        if length * fares[i] < 20:
            fancy = rides[i + 1]
    return(fancy)
