# Check if the given string is a correct time representation of the 24-hour clock.

def validTime(string):
    hour, minute = string.split(":")
    if int(hour) in range(0, 25):
        if int(minute) in range(0, 61):
            return(True)
    return(False)
