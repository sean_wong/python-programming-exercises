# Consider a sequence of numbers a0, a1, ..., an, in which an element is equal to the sum of squared digits of the previous element. The sequence ends once an element that has already been in the sequence appears again. Given the first element a0, find the length of the sequence.

def squareDigitSequence(a0):
    equal = False
    sequence = [a0]
    while not equal:
        squareSum = 0
        integer = sequence[-1]
        while integer:
            squareSum += (integer % 10) ** 2
            integer //= 10
        if squareSum in sequence:
            sequence.append(squareSum)
            equal = True
        else:
            sequence.append(squareSum)
    return(len(sequence))
