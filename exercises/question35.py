#Define a function which can generate and print a list where the values are square of numbers between 1 and 20 (both included).

def squares(n):
    squares = []
    for i in range(1, n):
        squares.append(i**2)
    return(squares)

print(squares(17))
