#Please write a binary search function which searches an item in a sorted list. The function should return the inde x of element to be searched in the list.

import math

given = [1,5,10,11,12,200,500,1000,1415]

def binarySearch(sortedList, element):
    first = 0
    last = len(sortedList)-1
    index = None
    while (last>=first) & (index == None):
        middle = math.floor((first + last) / 2)
        if sortedList[middle] == element:
            index = middle
        elif sortedList[middle] > element:
            last = middle - 1
        else:
            first = middle + 1
    return(index)

print(binarySearch(given, 12))
print(binarySearch(given, 200))
print(binarySearch(given, 1001))
