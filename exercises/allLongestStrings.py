# Given array of strings, return another array of longest strings

def allLongestStrings(strings):
    longest = []
    max_len = 0
    for i in strings:
        if len(i) > max_len:
            max_len = len(i)
    for i in strings:
        if len(i) == max_len:
            longest.append(i)
    return(longest)
