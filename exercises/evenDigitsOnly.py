# Check if all digits of the given integer are even

def evenDigitsOnly(integer):
    for i in str(integer):
        if int(i) % 2 == 1:
            return(False)
    return(True)
