#Write a function to compute 5/0 and use try/except to catch the exceptions.

def errors():
    return(5/0)

try:
    errors()
except(ZeroDivisionError):
    print("Error")
