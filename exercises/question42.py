#Write a program which accepts a string as input to print "Yes" if the string is "yes" or "YES" or "Yes", otherwise print "No".

string = input()
if (string == "yes") | (string == "YES") | (string == "Yes"):
    print("Yes")
else:
    print("No")
