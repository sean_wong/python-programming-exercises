# You will be provided with an initial array (the first argument in the destroyer function), followed by one or more arguments. Remove all elements from the initial array that are of the same value as these arguments.

def seekAndDestroy(*args):
    toBe = set(args[0])
    for i in args[1:]:
        toBe = toBe ^ set(i)
    return(list(toBe))
