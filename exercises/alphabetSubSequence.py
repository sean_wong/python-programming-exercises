# Check whether given string is a subsequence of the plaintext alphabet

def alphabetSubSequence(string):
    char_counts = {}
    for i in string:
        if i not in char_counts:
            char_counts[i] = 1
        else:
            char_counts[i] += 1
    for v in char_counts.values():
        if v > 1:
            return(False)
    return(True)
