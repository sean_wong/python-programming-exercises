# Translate provided string to pig latin

def pigLatin(string):
    vowels = ['a','e','i','o','u']
    if string[0] in vowels:
        return(string + "way")
    else:
        return(string[1:] + string[0] + "ay")
