# Given rectangular matrix and integers a and b, consider the union of the ath row and the bth column of the matrix. Return sum of all elements of that union.

def crossingSum(matrix, a, b):
    sums = 0
    for i in range(0, len(matrix)):
        for j in range(0, len(matrix[0])):
            if i == a or b == j:
                sums += matrix[i][j]
    return(sums)
