# Check if string ends with given target string

def confirmEnding(string, target):
    length_target = len(target)
    return(string[-length_target:] == target)
