# N candles are placed in a row, some of them are initially lit. For each candle from the 1st to the Nth the following algorithm is applied: if the observed candle is lit then states of this candle and all candles before it are changed to the opposite. Which candles will remain lit after applying the algorithm to all candles in the order they are placed in the line?

def switchLights(candles):
    for i in range(0, len(candles)):
        if candles[i]:
            for j in range(0, i + 1):
                if candles[j]:
                    candles[j] = 0
                else:
                    candles[j] = 1
    return(candles)
