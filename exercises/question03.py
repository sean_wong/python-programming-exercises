#With a given integral number n, write a program to generate a dictionary that contains (i, i*i) such that is an integral number between 1 and n (both included). and then the program should print the dictionary. Suppose the following input is supplied to the program:

number = int(input("What number? "))
squares = dict()
for num in range (1, number + 1):
    squares[num] = num**2

print(squares)
