# Given a one-digit number, you should find all unordered pairs of one-digit numbers whose values add up to the number

from math import ceil

def newNumeralSystem(num):
    nums = {'A': 0, 'B': 1, 'C': 2,
            'D': 3, 'E': 4, 'F': 5,
            'G': 6, 'H': 7, 'I': 8,
            'J': 9}
    possibilities = []
    for i in range(0, ceil(nums[num] / 2) + 1):
        contra = nums[num] - i
        possibilities.append(chr(65 + contra) + " + " + chr(65 + i))
    return(possibilities)
