# Given a string s, find and return the first instance of a non-repeating character. If no such character, return '_'

def firstNotRepeatingCharacter(string):
    counts = [0] * 26
    for i in string:
        counts[ord(i) - ord("a")] += 1
    for i in range(0, len(counts)):
        if counts[i] == 1:
            noRepeat = chr(i + 97)
            if string.index(chr(i + 97)) < string.index(noRepeat):
                noRepeat = chr(i + 97)
            return(noRepeat)
    return('_')
