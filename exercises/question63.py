#Write a program to compute:
#f(n)=f(n-1)+100 when n>0
#and f(0)=1
#with a given n input by console (n>0).

number = int(input("PLease enter a number: "))

def recursion(number):
    if number == 0:
        return(1)
    else:
        return(recursion(number - 1) + 100)

print(recursion(number))
