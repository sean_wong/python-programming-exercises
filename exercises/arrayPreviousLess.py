# Given array of integers, search among previous positions for last position containing a smaller value than i. Store value at position. If no such value, then store -1.

def arrayPreviousLess(integers):
    lesserValues = []
    for i in range(0, len(integers)):
        smallerValue = -1
        for j in range(0, i):
            if integers[j] < integers[i]:
                smallerValue = integers[j]
        lesserValues.append(smallerValue)
    return(lesserValues)
