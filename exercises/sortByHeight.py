# Some people are standing in a row in a park. There are trees between them which cannot be moved. Your task is to rearrange the people by their heights in a non-descending order without moving the trees

def sortByHeight(array):
    noTree = [i for i in array if i > 0]
    noTree.sort(reverse=True)
    tree_loc = [-1] * len(array)
    for i in range(0, len(array)):
        if array[i] != tree_loc[i]:
            tree_loc[i] = noTree.pop()
    return(tree_loc)
