# Each day a plant grows by upSpeed meters. Each night plant's height decreases by downSpeed meters. Initially, plant is 0 meters. How many data until plant reaches certain level.

def growingPlant(upSpeed, downSpeed, desiredHeight):
    plantHeight = 0
    days = 0
    while plantHeight < desiredHeight:
        plantHeight += upSpeed
        if plantHeight > desiredHeight:
            return(days)
        plantHeight -= downSpeed
        days += 1
