#Write a program which takes 2 digits, X,Y as input and generates a 2-dimensional array. The element value in the i-th row and j-th column of the array should be i*j.

dimensions = input("Enter the dimensions of the table: ")

dimensions = dimensions.split(',')
dimensions[0] = int(dimensions[0])  # length
dimensions[1] = int(dimensions[1])  # width

table = []

for row in range(dimensions[0]):
    row_nums = []
    for col in range(dimensions[1]):
        row_nums.append(row * col)
    table.append(row_nums)

print(table)
