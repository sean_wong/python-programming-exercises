# Check if array a and array b are similar. Two arrays are similar if one array can be obtained from another by swapping at most one pair of elements

def areSimilar(a, b):
    counter = 0
    for i in range(1, len(a)):
        if a[i] != b[i]:
            counter += 1
            holderA = a[i]
            a[i] = a[i-1]
            a[i-1] = holderA
        if counter > 1:
            return(False)
    return(True)

