# You find yourself in Bananaland trying to buy a banana. You are super rich so you have an unlimited supply of banana-coins, but you are trying to use as few coins as possible. The coin values available in Bananaland are stored in a sorted array coins. coins[0] = 1, and for each i (0 &lt; i&lt; coins.length) coins[i] is divisible by coins[i - 1]. Find the minimal number of banana-coins you'll have to spend to buy a banana given the banana's price.

def minimalNumberOfCoins(coins, price):
    numCoins = 0
    spend = 0
    while spend != price:
        for i in coins[::-1]:
            if spend + i <= price:
                spend += i
                numCoins += 1
                break
    return(numCoins)
