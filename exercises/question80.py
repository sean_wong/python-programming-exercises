#Please write a program to print the running time of execution of "1+1" for 100 times.

import time
#from timeit import Timer

past = time.time()
for i in range(100):
    1+1

now = time.time()
print(now-past)

#time = Timer("for i in range(100): 1 + 1")
#print(time.timeit())
