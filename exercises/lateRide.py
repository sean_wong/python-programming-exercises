# You go for ride at 00:00, bike's timer counts length of your ride. Using bike's timer, calculate the current time

import math

def lateRide(timer):
    hours = str(math.floor(timer / 60))
    mins = str(timer % 60)
    timeSum = 0
    for i in hours + mins:
        timeSum += int(i)
    return(timeSum)
