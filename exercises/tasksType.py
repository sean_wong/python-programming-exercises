# Given an array of deadlines and today's date day, your goal is to find the number of tasks with each label type and return it as an array with the format [Today, Upcoming, Later], where Today, Upcoming and Later are the number of tasks that correspond to that label.

def tasksType(deadlines, day):
    tasks = [0, 0, 0]
    for task in deadlines:
        if task <= day:
            tasks[0] += 1
        elif task > day and task < day + 7:
            tasks[1] += 1
        else:
            tasks[2] += 1
    return(tasks)
