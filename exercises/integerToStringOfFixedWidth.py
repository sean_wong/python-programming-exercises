# Given a positive integer number and a certain length, modify given number to have specified length.

def integerToStringOfFixedWidth(integer, length):
    string = str(integer)
    if len(string) < length:
        string = '0' * (length - len(string)) + string
    return(string[-length:])
