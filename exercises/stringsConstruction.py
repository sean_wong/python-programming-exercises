# How many strings equal to a can be constructed using letters from the string b? Each letter can be used only once and in one string only.

def stringsConstruction(a, b):
    letters = {}
    occurences = []
    for i in b:
        if i not in letters:
            letters[i] = 1
        else:
            letters[i] += 1
    for i in a:
        if i in letters:
            occurences.append(letters[i])
        else:
            return(0)
    return(min(occurences))
