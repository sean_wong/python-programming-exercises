# Given a year, return the century it is in

import math

def centuryFromYear(year):
    return(math.ceil(year / 100))
