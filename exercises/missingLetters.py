# Find missing letter in passed letter range and return it. If all letters present in range, return undefined.

def missingLetters(letters):
    alpha = 'abcdefghijklmnopqrstuvwxyz'
    first = ord(letters[0]) - ord('a')
    last = ord(letters[-1]) - ord('a')
    full = set(alpha[first:last + 1])
    missing = (full ^ set(letters))
    if len(missing) == 0:
        return(None)
    return(missing.pop())
