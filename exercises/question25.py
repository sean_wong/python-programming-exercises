#Define a class, which have a class parameter and have a same instance parameter.

class dog:
    behavior = "good boy"

    def __init__(self, breed, name):
        self.breed = breed
        self.name = name

lucky = dog("Shiba Inu", "Lucky")
print(lucky.breed)
print(lucky.name)
print(lucky.behavior)
