# Given array of integers, find maximal possible sum of k consecutive elements.

def arrayMaxConsecutiveSum(integers, k):
    max_sum = sum(integers[0:k])
    for i in range(1, len(integers) - k + 1):
        if sum(integers[i:i + k]) > max_sum:
            max_sum = sum(integers[i:i + k])
    return(max_sum)
