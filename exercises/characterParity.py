# Given a character, check if it represents an odd digit, an even digit or not a digit at all.

def characterParity(char):
    if char.isdigit():
        if int(char) % 2 == 1:
            return("odd")
        else:
            return("even")
    else:
        return("Not a digit")
