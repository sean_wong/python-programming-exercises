#Write a program which accepts a sequence of comma-separated numbers from console and generate a list and a tuple which contains every number.

sequence = input("Enter a sequence of numbers separated by commas: ")

num_list = sequence.split(",")
num_tuple = tuple(num_list)

print(num_list)
print(num_tuple)
