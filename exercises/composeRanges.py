# Given a sorted integer array without duplicates, return a summary of the number ranges it contains

def composeRanges(integers):
    ranges = []
    first = integers[0]
    for i in range(1, len(integers)):
        if integers[i - 1] + 1 != integers[i]:
            last = integers[i - 1]
            ranges.append(str(first) + "->" + str(last))
            first = integers[i]
    if len(integers) % 2 == 1:
        ranges.append(str(first))
    return(ranges)        
