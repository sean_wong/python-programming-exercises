# Given array of integers, find closest pair of elements that add up to sum. Return distance between closest pair, if no pair, return (-1)

def findClosestPair(integers, sumOf):
    max_distance = len(integers)
    distance = None
    for i in range(0, len(integers)):
        complement = sumOf - integers[i]
        if complement in integers:
            distance = abs(integers.index(complement) - i)
            if distance < max_distance:
                max_distance = distance
    if distance == None:
        return(-1)
    return(max_distance)
