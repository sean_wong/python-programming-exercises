# Given a string, find the number of different characters in it

def differentSymbolsNaive(string):
    different = {}
    for i in string:
        if i not in different:
            different[i] = 1
    return(len(different))
