# Correct a proper noun so it always begins with a capital letter

def properNounCorrection(noun):
    return(noun.title())
