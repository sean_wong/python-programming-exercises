#1.7
#Given an image represented by an N x N matrix, where each pixel in the image is represented by an integer, write a method to rotate the image by 90 degrees. Can you do this in place?

import math

def rotateMatrix(image):
    for i in range(math.floor(len(image)/2)):
        begin = i
        end = len(image) - 1 - i
        temp = []
        for j in range(begin, end):
            temp.append(image[i][j])    #top to temp
            image[i][j] = image[end-j+i][i]    #left to top
            image[end-j+i][i] = image[end][end-j+i]   #bottom to left
            image[end][end-j+i] = image[j][end]   #right to bottom
            image[j][end] = temp[j-i]   #top to right
    return(image)

#image = [['0', '1', '2', '3', '4'], ['5', '6', '7', '8', '9'], ['a', 'b', 'c', 'd', 'e'], ['f', 'g', 'h', 'i', 'j'], ['k', 'l', 'm', 'n', 'o']]

#for i in image:
#    print(i)

#print("\n")
#rotate180 = rotateMatrix(rotateMatrix(image))
#for i in rotate180:
#    print(i)
