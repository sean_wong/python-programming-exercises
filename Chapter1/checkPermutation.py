#1.2
#Given two strings, write a method to decide if one is a permutation of the other.

def checkPermutation(string1, string2):
    if sorted(string1) == sorted(string2):
        return(True)
    return(False)

#print(checkPermutation("hello", "hi"))  #False
#print(checkPermutation("hello", "olleh"))   #True
