#1.5
#There are three types of edits that can be performed on strings: insert a character, remove a character, or replace a character. Given two strings, write a function to check if they are one edit (or zero edits) away.

import math

def oneAway(string1, string2):
    if math.fabs(len(string1) - len(string2)) >= 2:
        return(False)
    elif len(string1) == len(string2):
        return(sameLength(string1, string2))
    elif len(string1) > len(string2):
        return(diffLength(string1, string2))
    else:
        return(diffLength(string2, string1))

def sameLength(string1, string2):
    count = 0
    for i in range(len(string1)):
        if string1[i] != string2[i]:
            count += 1
            if count > 1:
                return(False)
    return(True)

def diffLength(string1, string2):
    index = 0
    count = 0
    while i < len(string2):
        if string1[i + count] == string2[i]:
            i += 1
        else:
            count += 1
            if count > 1:
                return(False)
    return(True)

#print(oneAway("pale", "ple"))   #True
#print(oneAway("pales", "pale")) #True
#print(oneAway("pale", "bale"))  #True
#print(oneAway("pale", "bake"))  #False
