#1.6
#Implement a method to perform basic string compression using the counts of repeated characters. For example, the string aabcccccaaa would become a2b1c5a3. If the "compressed" string would not become smaller than the original string, your method should return the original string. You can assume the string has only uppercase and lowercase letters (a-z).

def stringCompression(string):
    compressed = ""
    occured = 1
    try:
        for i in range(len(string)):
            character = string[i]
            if string[i] == string[i+1]:
                occured += 1
            else:
                compressed += "{}{}".format(character, occured)
                occured = 1
    except IndexError:
        compressed += "{}{}".format(character, occured)
    
    if len(compressed) < len(string):
        return(compressed)
    else:
        return(string)

#print(stringCompression("aabcccccaaa")) #aabcccccaaa
#print(stringCompression("hello"))   #hello
