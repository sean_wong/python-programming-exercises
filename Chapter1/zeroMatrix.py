#1.8
#Write an algorithm such that if an element in an M x N matrix is 0, its entire row and column are set to 0.

import numpy as np

def zeroMatrix(matrix):
    matrix = np.array(matrix)
    colCoords = []
    rowCoords = []
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            if matrix[i][j] == 0:
                colCoords.append(j)
                rowCoords.append(i)
        matrix[rowCoords,:] = 0
    matrix[:, colCoords] = 0
    return(matrix)

#matrix = [[1,2,3,4,5,6,7,8,9,0],[3,4,6,4,2,2,4,6,6,5],[0,3,4,6,4,2,4,5,6,4],[2,21,2,3,4,6,5,4,5,6],[2,3,5,7,2,1,3,5,76,3]]

#print(zeroMatrix(matrix))
