#1.4
#Given a string, write a function to check if it is a permutation of a palindrome. A palindrome is a word or phrase that is the same forwards and backwards. A permutation is a rearrangement of letters. The palindrome does not need to be limited to just dictionary words. You can ignore casing and non-letter characters.

def palindromePermutation(string):
    string = string.replace(" ", "")
    string = string.lower()
    characters = {}

    for c in string:
        if c not in characters:
            characters[c] = 1
        else:
            characters[c] += 1
    
    counter = 0
    for i in characters:
        counter += characters[i] % 2 == 1

    if (counter == 0) | (counter == 1):
        return(True)
    return(False)

#print(palindromePermutation("racecare"))   #True
#print(palindromePermutation("acotcat")) #True
#print(palindromePermutation("hello"))   #False
