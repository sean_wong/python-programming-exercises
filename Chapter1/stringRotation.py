#1.9
#Assume you have a method isSubstring which checks if one word is a substring of another. Given two strings, s1 and s2, write code to check if s2 is a rotation of s1 using only one call to isSubstring (e.g., "waterbottle" is a rotation of "erbottlewat").

def isSubstring(string1, string2):
    return(string2 in string1)

def stringRotation(string1, string2):
    return(isSubstring(string2+string2,string1))

#print(stringRotation("waterbottle", "erbottlewat")) #True
#print(stringRotation("waterbottle", "botwatertle")) #False
